-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jul 24, 2022 at 01:30 PM
-- Server version: 8.0.29
-- PHP Version: 8.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tp`
--

-- --------------------------------------------------------

--
-- Table structure for table `lab18r01_users`
--

CREATE TABLE IF NOT EXISTS `lab18r01_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` char(100) NOT NULL,
  `password` char(255) NOT NULL,
  `firstname` char(100) NOT NULL,
  `lastname` char(100) NOT NULL,
  `role` enum('user','admin') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lab18r01_users`
--

INSERT INTO `lab18r01_users` (`id`, `username`, `password`, `firstname`, `lastname`, `role`) VALUES
(1, 'administrator', 'c620ca92831a71e13a03d7d293efaa33', 'System', 'Administrator', 'admin'),
(2, 'student1', '7c6a180b36896a0a8c02787eeafb0e4c', 'Luc', 'Marcel', 'user');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
