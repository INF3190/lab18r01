<?php
namespace Labauth;
require 'Medoo.php';
require 'config.php';
use Medoo\Medoo;

class Lab18r01{
    const LOGINPAGE='login.php';
    const DASHBOARD='connected.php';
    const CONNECTED='connected';
    const LOGINFORM='loginform';
    const LOGOUTFORM='logoutform';
    private $database=null;
    private $message="";

    function __construct(){
     global $CFG;
     $this->database = new Medoo($CFG);
    }
    
    function require_login(){
        if(!$_SESSION[self::CONNECTED]){
          header("location: ".self::LOGINPAGE."");
         exit();
        } 

    }

    function redirect_connected(){
        if($_SESSION[self::CONNECTED]){
          header("location: ". self::DASHBOARD."");
         exit();
         } 
    }
    /*
    * Ici, on verifie  si on a soumis le formulaire
    */
    private function login(){

        if($_REQUEST[self::LOGINFORM]){
         $username=trim($_REQUEST['login']);
         $password=trim($_REQUEST['password']);
         $fields='*';
         $where=array();
         $where['username']="$username";
         $where['password']=md5($password);
         $record=$this->database->select('users',"$fields",$where);
         
         if($record[0]){
            $currentuser=$record[0];
            $_SESSION['username']=$currentuser['username'];
            $_SESSION['firstname']=$currentuser['firstname'];
            $_SESSION['role']=$currentuser['role'];
            $_SESSION[self::CONNECTED]=1;

            $this->redirect_connected();
         }
         $this->message="<h2 class='alert'>login ou mot de passe incorrect<h2>";

        }
        }

    private function logout(){
        if($_SESSION[self::CONNECTED]){
          session_destroy();
         } 
         header("location: ". self::LOGINPAGE."");
         exit();
    }

    function submit(){
        if($_REQUEST[self::LOGINFORM]){
            $this->login();
        }elseif($_REQUEST[self::LOGOUTFORM]){
            $this->logout();
        }
    }

    function display_message(){
        if($this->message){
            echo $this->message;
            $this->message=null;
        }
    }
}

?>